package net.atos.testing;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class CalculatorTest {
    private final Calculator subject = new Calculator();
    
    @Test
    public void should_return_2_for_plus_times_over_minus_plus_plus() throws Exception {
        // GIVEN
        String operation = "+*/-++";
        double expected = 2.;

        // WHEN
        double actual = subject.evaluate(operation);

        // THEN
        Assertions.assertThat(actual).isEqualTo(expected);
    }

    @Test(expected = UnknownOperationException.class)
    public void should_throw_exception_for_invalid_operation() throws Exception {
        // GIVEN
        String operation = "+*log-++";

        // WHEN
        subject.evaluate(operation);

        // THEN
        // It should throw invalid operation exception
    }
}
